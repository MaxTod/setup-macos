#!/usr/bin/env bash

# Close any open System Preferences panes, to prevent them from overriding
# settings we’re about to change
osascript -e 'tell application "System Preferences" to quit'

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macos` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

###############################################################################
# General UI/UX                                                               #
# show own properties with : defaults read NSGlobalDomain                     #
###############################################################################

# Disable the sound effects on boot
#sudo nvram SystemAudioVolume=" "

# Set global interface style
## Light
#defaults delete NSGlobalDomain AppleInterfaceStyle; defaults delete NSGlobalDomain AppleInterfaceStyleSwitchesAutomatically
## Dark
defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"
## Automatic
#defaults write NSGlobalDomain AppleInterfaceStyle -string "Dark"; defaults write NSGlobalDomain AppleInterfaceStyleSwitchesAutomatically -int 1;

# Set Accent color
#  this setting defines two properties:
#    - AppleAccentColor
#    - AppleAquaColorVariant
#  it also presets AppleHighlightColor, but this can be overriden
#  note that AppleAquaColorVariant is alway "1" except for "Graphite", where it is "6".
#  note that the AccentColor "Blue" is default (when there is no entry) and has no AppleHighlightColor definition.
#
#  Color    AppleAquaColorVariant    AccentColor    AppleHighlightColor
#    Red            1                   0           "1.000000 0.733333 0.721569 Red"
#    Orange         1                   1           "1.000000 0.874510 0.701961 Orange"
#    Yellow         1                   2           "1.000000 0.937255 0.690196 Yellow"
#    Green          1                   3           "0.752941 0.964706 0.678431 Green"
#    Purple         1                   5           "0.968627 0.831373 1.000000 Purple"
#    Pink           1                   6           "1.000000 0.749020 0.823529 Pink"
#    Blue           deleted             deleted     deleted
#    Graphite       6                   -1          "0.847059 0.847059 0.862745 Graphite"
#
## Set accent color to Red
#defaults write NSGlobalDomain "AppleAquaColorVariant" -int 1
#defaults write NSGlobalDomain "AccentColor" -int 0
## Set highlight color to Red
#defaults write NSGlobalDomain AppleHighlightColor -string "1.000000 0.733333 0.721569 Red"

# Set sidebar icon size: 1 (small), 2 (medium), 3 (large)
defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2

# Show / Hide menu bar: 0 (show), 1 (hide)
defaults write NSGlobalDomain _HIHideMenuBar -int 0

# Scrollbars appearance: WhenScrolling, Automatic, Always
defaults write NSGlobalDomain AppleShowScrollBars -string "Always"

# Scrollbars behavior: 0(go to next page), 1(go to clicked place)
defaults write NSGlobalDomain AppleScrollerPagingBehavior -int 1

# Update or close docs confirmation: false(not confirm), true(confirm)
defaults write NSGlobalDomain NSCloseAlwaysConfirmsChanges -bool false

# Close windows when closing app: false(don't close), true(close)
defaults write NSGlobalDomain NSQuitAlwaysKeepsWindows -bool true

#####

# Disable automatic capitalization as it’s annoying when typing code
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

# Disable smart dashes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

# Disable automatic period substitution as it’s annoying when typing code
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

# Disable smart quotes as they’re annoying when typing code
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

# Disable auto-correct
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

# Set window tabbing mode: always, manual, fullscreen
defaults write NSGlobalDomain AppleWindowTabbingMode -string "fullscreen"

# Set action on double click on window main bar: None, Maximize, Minimize
defaults write NSGlobalDomain AppleActionOnDoubleClick -string "Maximize"

###############################################################################
# Trackpad, mouse, keyboard, Bluetooth accessories, and input                 #
###############################################################################

# Trackpad: enable tap to click for this user and for the login screen
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

# Increase sound quality for Bluetooth headphones/headsets
defaults write com.apple.BluetoothAudioAgent "Apple Bitpool Min (editable)" -int 40

# Disable press-and-hold for keys in favor of key repeat
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false

# Set a blazingly fast keyboard repeat rate
defaults write NSGlobalDomain KeyRepeat -int 1
defaults write NSGlobalDomain InitialKeyRepeat -int 10

###############################################################################
# Finder                                                                      #
###############################################################################

# Set Desktop as the default location for new Finder windows
# For other paths, use `PfLo` and `file:///full/path/here/`
defaults write com.apple.finder NewWindowTarget -string "PfDe"
defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}/Desktop/"

# Handle icons for hard drives, servers, and removable media on the desktop
defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool false
defaults write com.apple.finder ShowHardDrivesOnDesktop -bool false
defaults write com.apple.finder ShowMountedServersOnDesktop -bool false
defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool false

# Finder: show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Keep folders on top when sorting by name
defaults write com.apple.finder _FXSortFoldersFirst -bool true

# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# Disable the warning when changing a file extension
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Avoid creating .DS_Store files on network or USB volumes
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

# Enable snap-to-grid for icons on the desktop and in other icon views
/usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist

# Use list view in all Finder windows by default
# Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

# Disable the warning before emptying the Trash
defaults write com.apple.finder WarnOnEmptyTrash -bool false

###############################################################################
# Dock, Dashboard, and hot corners                                            #
###############################################################################

# Set the icon size of Dock items
# recommended values between 16 and 128
defaults write com.apple.dock tilesize -int 38

# Set the Dock magnification when mouse hover it
defaults write com.apple.dock magnification -bool false
# Set the Dock magnification size, recommended values between 16 and 128
#defaults write com.apple.dock largesize -float 64

# Set the Dock orientation: left, bottom, right
defaults write com.apple.dock orientation -string "bottom"

# Maximize or minimize app in Dock icon: true, false
defaults write com.apple.dock minimize-to-application -bool false

# Activate app launch animation : true, false
defaults write com.apple.dock launchanim -bool true

# Hide or show Dock: true, false
defaults write com.apple.dock autohide -bool false

# Show or hide opened app under Dock icon: true, false
defaults write com.apple.dock show-process-indicators -bool true

# Show or hide recently opened app in Dock: true, false
defaults write com.apple.dock show-recents -bool false

# Wipe all (default) app icons from the Dock
# This is only really useful when setting up a new Mac, or if you don’t use
# the Dock to launch apps.
defaults write com.apple.dock persistent-apps -array

###############################################################################
# Photos                                                                      #
###############################################################################
# Prevent Photos from opening automatically when devices are plugged in
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool true

###############################################################################
# Messages                                                                    #
###############################################################################
# Disable automatic emoji substitution (i.e. use plain text smileys)
defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "automaticEmojiSubstitutionEnablediMessage" -bool false
# Disable smart quotes as it’s annoying for messages that contain code
defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "automaticQuoteSubstitutionEnabled" -bool false
# Disable continuous spell checking
defaults write com.apple.messageshelper.MessageController SOInputLineSettings -dict-add "continuousSpellCheckingEnabled" -bool false

###############################################################################
# Kill affected applications                                                  #
###############################################################################
for app in "Activity Monitor" \
    "cfprefsd" \
    "Dock" \
    "Finder" \
    "Google Chrome" \
    "Messages" \
    "Photos" \
    "Safari" \
    "SystemUIServer"; do
    killall "${app}" &> /dev/null
done
